package com.davidjebavy.exchangerates.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="exchange_rates")
@Data
public class ExchangeRate {

    @Id
    private String name;
    private String amount;
    private float cnbMid;
    private String country;
    private float currBuy;
    private float currMid;
    private float currSell;
    private float ecbMid;
    private float move;
    private String shortName;
    private float valBuy;
    private float valMid;
    private float valSell;
    private LocalDateTime validFrom;
    private int version;

}
