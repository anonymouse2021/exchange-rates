package com.davidjebavy.exchangerates.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UrlBuilder {

    private String webApiKey;
    private String URL;

    public String getUrl() {
        return URL + "?" + "web-api-key=" + webApiKey;
    }
}
