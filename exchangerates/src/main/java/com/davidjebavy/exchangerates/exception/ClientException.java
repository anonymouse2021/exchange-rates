package com.davidjebavy.exchangerates.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ClientException extends RuntimeException {
    private HttpStatus status;
    private Response response;

    public ClientException(String message, HttpStatus status) {
        this.status = status;
        this.response = new Response(message, status);
    }

    /**
     * Inner class for server response.
     * ClientException include stackTrace as it extends RuntimeException.
     * This class is just wrapper for message and status code.
     */
    @Data
    @AllArgsConstructor
    private class Response {
        private final HttpStatus status = HttpStatus.BAD_GATEWAY;
        private String message;
        private HttpStatus ResourceServerResponseStatus;

    }
}
