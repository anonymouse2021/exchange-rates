package com.davidjebavy.exchangerates.controller;

import com.davidjebavy.exchangerates.entity.ExchangeRate;
import com.davidjebavy.exchangerates.service.ExchangeRatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ExchangeRateController {

    @Autowired
    private ExchangeRatesService exchangeRatesService;

    @GetMapping("/rates")
    @CrossOrigin
    public List<ExchangeRate> rates(@PathParam("update") boolean update) {
        return exchangeRatesService.getRates(update);
    }

}
