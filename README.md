# exchange-rates
This is DEMO project displaying exchange rates.

# Start
<h2>DB</h2>
First mysql need to be set up and DB has to be created. The script is included in exchagerates/src/main/resources/sql_scripts/sql_scripts.sql<br>
SQL should run on localhost:3306 - This can be changed in application.yml
<br>
<h2>Backend</h2>
Run as Spring Boot application.<br>
BE has just one endpoint: /api/rates<br>
By default BE will run on localhost:8080 - Can be changed in application.yml with property server.port: port_number
<br>
<h2>Front-end</h2> 
React application rendering data from BE.<br>
Navigate to folder exchangerates-fe/ and run npm start.
This should start server on localhost:3000
