import React, { Component } from 'react'
import './Table.css';

class Table extends Component {
   constructor(props) {
      super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
      this.state = { //state is by default an object
         rates: [],
         isLoaded: false,
      }
   }

   componentDidMount() {
       fetch('http://localhost:8080/api/rates?update=false')
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    rates: json,
                })
            })
   }

   renderTableData() {
    return this.state.rates.map((rate, index) => {
       const { name, country, currBuy, currSell, move, shortName, validFrom} = rate //destructuring
       return (
          <tr class="table-row">
             <td>{name}</td>
             <td>{shortName}</td>
             <td>{move}</td>
             <td>{country}</td>
             <td>{currBuy}</td>
             <td>{currSell}</td>
             <td>{validFrom}</td>
          </tr>
       )
    })
 }

   render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.
      return (
         <div class="wrapper">
            <h1 id="header">Exchange rates</h1>
            <div class="container">
                <table class="rates">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>country</th>
                            <th>currBuy</th>
                            <th>currSell</th>
                            <th>move</th>
                            <th>shortName</th>
                            <th>validFrom</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTableData()}
                    </tbody>
                </table>
            </div>
         </div>
      )
   }
}

export default Table //exporting a component make it reusable and this is the beauty of react